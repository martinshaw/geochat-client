# geochat-client
## Geolocation-based messaging and media content sharing platform

For the accompanying server platform: https://gitlab.com/martinshaw/geochat-server

*Copyright (C) Martin David Shaw - All Rights Reserved.*
*Unauthorized copying of this file, via any medium is strictly prohibited, proprietary and confidential.*
*Written by Martin David Shaw <15090190@stu.mmu.ac.uk> <thirdyearproject@martinshaw.co>, November 2017.*
 
